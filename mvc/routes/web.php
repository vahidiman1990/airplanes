<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FlightController@index');
Route::get('/flight/show/{id}', 'FlightController@show');
Route::get('/reserve/show', 'ReserveController@show');
Route::post('/reserve/ticket', 'ReserveController@reserve');
