<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="/bootstrap/css/bootstrap-grid.css" />
    <link rel="stylesheet" href="/bootstrap/css/bootstrap-reboot.css" />
    <script src="/bootstrap/js/bootstrap.js"></script>
    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #222;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .flex-center {
            width: 50%;
            padding:10px;
            text-align: center;
            float:left;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
    <div style="display: inline-block; background-color: #f2f2f2; width:100%;">
        <div class="flex-center">
            <table class="table table-bordered">
                <tr>
                    <th>From</th>
                    <th>To</th>
                    <th>Date</th>
                    <th>Time</th>
                </tr>
                <tr>
                @foreach($flight as $f)
                    <tr>
                        <td>{{ $f->from }}</td>
                        <td>{{ $f->to }}</td>
                        <td>{{ $f->date }}</td>
                        <td>{{ $f->time }}</td>
                    </tr>
                    @endforeach
                    </tr>
            </table>
        </div>
        <div style="width:30%; float:left; margin:10px; padding:10px;">
            <form action="/reserve/ticket" method="post">
                {!! @csrf_field() !!}
                <table class="table">
                    <tr>
                        <td>Name</td>
                        <td><input type="text" name="txt_name" class="form-control" /> </td>
                    </tr>
                    <tr>
                        <td>Family</td>
                        <td><input type="text" name="txt_family" class="form-control" /> </td>
                    </tr>
                </table>
                <input type="submit" value="Reserve" class="btn btn-success"/>
            </form>
        </div>
    </div>
</body>
</html>
