@extends('master')
@section('tickets')
    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Family</th>
        </tr>
        @foreach($reserve as $r)
            <tr>
                <td>{{ $loop->index +1 }}</td>
                <td>{{ $r->name }}</td>
                <td>{{ $r->family }}</td>
            </tr>
        @endforeach
    </table>
@endsection