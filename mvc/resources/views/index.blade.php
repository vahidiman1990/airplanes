<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="/bootstrap/css/bootstrap-grid.css" />
        <link rel="stylesheet" href="/bootstrap/css/bootstrap-reboot.css" />
        <script src="/bootstrap/js/bootstrap.js"></script>
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #222;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
                padding:10px;
                text-align: center;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
    <a href="/reserve/show" style="padding:5px;">Reseve tickets</a>
        <div class="flex-center">
            <table class="table table-bordered">
                <tr>
                    <th>From</th>
                    <th>To</th>
                    <th>Date</th>
                    <th>Time</th>
                </tr>
                @foreach($flights as $flight)
                    <tr>
                        <td>{{ $flight->from }}</td>
                        <td>{{ $flight->to }}</td>
                        <td>{{ $flight->date }}</td>
                        <td>{{ $flight->time }}</td>
                        <td><a href="/flight/show/{{$flight->id}}">Reserve</a></td>
                    </tr>
                @endforeach
            </table>
        </div>

        <div class="flex-center">
        </div>
    </body>
</html>
