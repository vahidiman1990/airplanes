<?php

use Faker\Generator as Faker;

$factory->define(App\Flight::class, function (Faker $faker) {
    return [
        'from' => $faker->city ,
        'to' => $faker->city ,
        'date' => $faker->date('Y-m-d') ,
        'time' => $faker->time('H:i:s') ,
    ];
});
