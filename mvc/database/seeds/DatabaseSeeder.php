<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        //$this->call(FlightSeeder::class);
        factory(\App\Flight::class , 15)->create();
        factory(\App\User::class , 10)->create();
    }
}
