<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FlightSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker)
    {
        for($i=0 ; $i<=15; $i++){
            DB::table('flights')->insert([
                'from' => $faker->city ,
                'to' => $faker->city ,
                'date' => $faker->date('Y-m-d') ,
                'time' => $faker->time('H:i:s') ,
            ]);
        }
    }
}
