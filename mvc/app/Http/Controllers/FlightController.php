<?php

namespace App\Http\Controllers;

use App\Flight;
use Illuminate\Http\Request;

class FlightController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $flights = Flight::display();
        return view('index' , compact('flights'));
    }

    public function show($id){
        $flight = Flight::findById($id);
        return view('reserve', compact('flight'));
    }
}
