<?php

namespace App\Http\Controllers;

use App\Reserve;
use Illuminate\Http\Request;

class ReserveController extends Controller
{
    public function reserve(){
        $val = [
            'name' => request('txt_name'),
            'family' => request('txt_family')
        ];
        Reserve::reserve_store($val);
        return redirect('/');

    }

    public function show(){
        $reserve = Reserve::show();
        return view('ticket' , compact('reserve'));
    }
}
