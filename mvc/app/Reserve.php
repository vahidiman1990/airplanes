<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserve extends Model
{
    protected $fillable = [
        'name', 'family'
    ];

    public static function reserve_store($val){
        return Reserve::create($val);
    }

    public static function show(){
        return Reserve::all();
    }
}
