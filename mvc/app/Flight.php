<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Flight extends Model
{
    public static function display(){
        return DB::table('flights')->get();
    }
    public static function findById($id){
        $res =  DB::table('flights')->where('id' , $id)->get();
        return $res;
    }
}
